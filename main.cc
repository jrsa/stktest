#include <stk/SineWave.h>
#include <stk/FileWvOut.h>

int main(int argc, char *argv[])
{
    stk::FileWvOut output_file("testout.wav", 1);
    stk::SineWave sin_osc;

    sin_osc.setFrequency(440);

    for (int i = 0; i < 100000; i++) {
        output_file.tick(sin_osc.tick());
    }

    return(0);
}